# Dungeon generator for the ttrpg Green Dawn Mall

Gdm generator is a tool for procedurally generating a world based on the role-playing game [Green Dawn Mall](https://comemartin.itch.io/green-dawn-mall) by Côme Martin.

Not ordered to-do list :

- option to skip to heart 
- settings menu
- button in heart of the mall to activate strange level decreasing
- show up or down direction before entering special levels
- show up or down direction on mechanical stairs
- allow to change mechanical stairs into special levels access
- avoid double first roll 1 on hallway counts to avoid being blocked at the start
- more effects / animations / micro-interactions
- Display hovered Shops / hallways on GM screen to player screen
- option to display details in the center of the squares
- add visuals to special levels player screen (no idea what though)
- option to save/load games
- roll history on Objects / Encounters / Themes / clues buttons
- indicate when a shop is identical to another one / has higher strange level
- allow for real dice rolls
- world map ( no idea how to display squares that would end up on the same spot )
- reroll buttons
- add a new game rule : squares with many hallways can lead to compatible previously visited squares for maximum confusion

Done :

- display modes ( players or GM )
- map display
- square details
- have the game's data
- shop interiors
- shop repetition pattern rules
- hallway events & stairs
- theme table
- encounters
- strange objects
- cool start menu
- player screen table/wall toggle
- factions
- friend clues
- upper & lower levels
- english version
