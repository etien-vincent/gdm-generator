import encounters from './encounters.json'
import events from './events.json'
import factions from './factions.json'
import friendClues from './friendClues.json'
import lowerLevels from './lowerLevels.json'
import menus from './menus.json'
import shopDescriptions from './shopDescriptions.json'
import shopDetails from './shopDetails.json'
import shopNames from './shopNames.json'
import shopNpcs from './shopNpcs.json'
import shopStrangeDescriptions from './shopStrangeDescriptions.json'
import shopStrangeObjects from './shopStrangeObjects.json'
import squareDetails from './squareDetails.json'
import strangeObjects from './strangeObjects.json'
import themes from './themes.json'
import upperLevels from './upperLevels.json'

const resources = {
  encounters,
  menus,
  events,
  factions,
  upperLevels,
  themes,
  strangeObjects,
  friendClues,
  lowerLevels,
  shopDescriptions,
  shopDetails,
  shopNames,
  shopStrangeDescriptions,
  squareDetails,
  shopNpcs,
  shopStrangeObjects
}

export default resources
