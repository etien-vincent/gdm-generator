import './assets/main.css'

import { createApp } from 'vue'
import i18next from 'i18next'
import I18NextVue from 'i18next-vue'
import App from './App.vue'
import resourcesFr from './i18n/fr/resources'
import resourcesEn from './i18n/en/resources'
import LanguageDetector from 'i18next-browser-languagedetector'

i18next.use(LanguageDetector).init({
  interpolation: {
    escapeValue: false
  },
  fallbackLng: 'fr',
  resources: {
    fr: { translation: resourcesFr },
    en: { translation: resourcesEn }
  }
})

//   i18next.changeLanguage()

createApp(App).use(I18NextVue, { i18next }).mount('#app')
