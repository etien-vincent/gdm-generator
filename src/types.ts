export interface IHallway {
  id: number
  squareIds: number[]
  direction: string
  eventNumber: number
  toOtherLevel: boolean
  goesUp?: boolean
  toSpecialLevel: boolean
  specialLevelId?: number
}
export interface IShop {
  shopNumber: number
  id: number
  detailNumber: number
  strangeLevel: number
  position: string
  npcNumber: number
  objectNumber: number
}
export interface ISquare {
  id: number
  strangeLevel: number
  shopCount: number
  hallwayCount: number
  squareDetailNumber: number
  shops: number[]
  hallways: number[]
  eventNumber: number
  specialLevelsId?: number
}
export interface IGameState {
  squareId: number
  hallwayId: number
  shopId: number
  shops: IShop[]
  squares: ISquare[]
  hallways: IHallway[]
  currentSquareId: number
  strangeObjectNumber: number
  encounterNumber: number
  factionNumber: number
  factionNpc: number
  themeNumber: number
  fakeFriendClue: number
  friendClue: boolean
  displayTitleScreen: boolean
  wallMode: boolean
  heartFound: boolean
  insideSpecialLevel: boolean
  specialLevels: ISpecialLevels[]
  specialLevelsId: number
  currentSpecialLevelsId: number
  currentSpecialLevelIndex: number
}

export interface ISpecialLevels {
  id: number
  isUpper: boolean
  levels: ISpecialLevel[]
  entrySquareId: number
}

export interface ISpecialLevel {
  typeNumber: number
  strangeLevel: number
  isUpper: boolean
  hasExit: boolean
  hasMallAccess: boolean
  eventsCount: number
  events: number[]
  npcNumber: number
  strangeObjectNumber: number
}

export interface IhallwayPositionsMap {
  [index: string]: string
}

export interface IDataRollTable {
  [index: string]: string
}

export interface IDataShopStrangeLevels {
  [index: string]: { [index: string]: string }
}

export interface IDataFactions {
  [index: string]: {
    name: string
    description: string
    likes: string
    dislikes: string
    npcs: { [index: string]: string }
  }
}

export interface IDataSpecialLevels {
  [index: string]: {
    name: string
    description: string
    strangeLevels: { [index: string]: string }
    strangeObject: { [index: string]: string }
    npcs: { [index: string]: string }
  }
}
